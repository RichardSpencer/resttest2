package com.mytest.resttest2.web;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.mytest.resttest2.entity.Offer;
import com.mytest.resttest2.exception.OfferNotFoundException;
import com.mytest.resttest2.service.OfferService;


@RestController
@RequestMapping("/resttest")
public class MyRestController {
	
	Logger log = LoggerFactory.getLogger(MyRestController.class);
	
    private OfferService offerService;

    @Autowired
    public void setOfferService(OfferService offerService) { this.offerService = offerService; }

    @GetMapping("/offers")
    public ResponseEntity<List<Offer>> getAllOffers() {
    	log.info("GET offers");
        List<Offer> list = offerService.listOffers();
        return new ResponseEntity<List<Offer>>(list, HttpStatus.OK);
    }

    @GetMapping("/offer/{id}")
    public ResponseEntity<Offer> getOffer(@PathVariable("id") long id) {
    	log.info("GET offer " + id);
        try {
            return new ResponseEntity<Offer>(offerService.findOffer(id), HttpStatus.OK);
        } catch (OfferNotFoundException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Offer Not Found");
        }
    }
    
    @PostMapping("/offer/addOffer") 
    public ResponseEntity<Offer> addOffer(@RequestBody Offer offer) {
    	log.info("ADD offer " + offer.toString());
    	return new ResponseEntity<Offer>(offerService.addOffer(offer), HttpStatus.OK);
    }
    
    @PutMapping("/offer/expireOffer/{id}") 
    public ResponseEntity<Offer> expireOffer(@PathVariable("id") long id) {
    	log.info("EXPIRE offer " + id);
        try {
            return new ResponseEntity<Offer>(offerService.expireOffer(id), HttpStatus.OK);
        } catch (OfferNotFoundException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Offer Not Found");
        }
    }
    
   
}
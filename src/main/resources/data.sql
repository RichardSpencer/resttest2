INSERT INTO offer (offer_id, name, description, start_time, end_time, offer_price, currency, hours)
VALUES (1, 'Woogle','Woogle offer', '2019-01-01 00:00:00', '2019-12-31 00:00:00', 99.99, 'GBP', 5);
INSERT INTO offer (offer_id, name, description, start_time, end_time, offer_price, currency, hours)
VALUES (2, 'Foogle','A frugal offer', '2019-01-01 00:00:00', '2019-12-31 00:00:00', 0.01, 'GBP', 1000);

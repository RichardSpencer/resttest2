package com.mytest.resttest2.web;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mytest.resttest2.entity.Offer;
import com.mytest.resttest2.service.OfferService;

@RunWith(SpringRunner.class)
@WebMvcTest(MyRestController.class)
public class MyRestControllerTest {
	
	private static final int ANY_DURATION = 10;
	private static final String ANY_CURRENCY = "GBP";
	private static final BigDecimal ANY_PRICE = BigDecimal.ONE;
	private static final LocalDateTime ANY_TIME = LocalDateTime.now();
	private static final String ANY_DESCRIPTION = "this_description";
	private static final String ANY_NAME = "this_name";

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private OfferService offerService;
	
	private Offer anyOffer;
	private ObjectMapper om = new ObjectMapper();
	
	@Before
	public void setUp() throws Exception {
		anyOffer = new Offer(ANY_NAME, ANY_DESCRIPTION,
				ANY_TIME, ANY_PRICE, ANY_CURRENCY, ANY_DURATION);
		// This is required otherwise LocalDateTime cannot be parsed correctly in the test context
		om.findAndRegisterModules();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test_getAllOffers() throws Exception {
		mockMvc.perform(get("/resttest/offers/"))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
		.andExpect(content().json("[]"));
		
		verify(offerService, times(1)).listOffers();
	}

	@Test
	public void test_getSingleOffer() throws Exception {
		when(offerService.findOffer(1)).thenReturn(anyOffer);
		MvcResult result = mockMvc.perform(get("/resttest/offer/1/"))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
		.andReturn();
		String content = result.getResponse().getContentAsString();
		
		assertTrue(content.contains(ANY_NAME));
		verify(offerService, times(1)).findOffer(1);
	}
	
	@Test
	public void test_addOffer() throws Exception {
		
		when(offerService.addOffer(any(Offer.class))).thenReturn(anyOffer);
		
		String json = om.writeValueAsString(anyOffer);
				
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/resttest/offer/addOffer/")
				  .content(json)
				  .contentType(MediaType.APPLICATION_JSON)
				  .accept(MediaType.APPLICATION_JSON))
				  .andExpect(status().isOk())
				  .andReturn();
		
		String content = result.getResponse().getContentAsString();
		assertTrue(content.contains(ANY_NAME));
		verify(offerService, times(1)).addOffer(any(Offer.class));
	}	
	
	@Test
	public void test_expireOffer() throws Exception {
		when(offerService.expireOffer(1)).thenReturn(anyOffer);
				
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.put("/resttest/offer/expireOffer/1")
				  .accept(MediaType.APPLICATION_JSON))
				  .andExpect(status().isOk())
				  .andReturn();
		
		String content = result.getResponse().getContentAsString();
		assertTrue(content.contains(ANY_NAME));
		verify(offerService, times(1)).expireOffer(1);
	}	
}

package com.mytest.resttest2.exception;

public class OfferNotFoundException extends RuntimeException {

    public OfferNotFoundException(String exception) {
        super(exception);
    }
}

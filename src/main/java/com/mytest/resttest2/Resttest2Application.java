package com.mytest.resttest2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Resttest2Application {

	public static void main(String[] args) {
		SpringApplication.run(Resttest2Application.class, args);
	}

}

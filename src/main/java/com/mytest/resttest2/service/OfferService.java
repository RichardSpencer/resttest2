package com.mytest.resttest2.service;

import java.util.List;
import com.mytest.resttest2.entity.Offer;


public interface OfferService {
    List<Offer> listOffers();
    Offer findOffer(long id);
    Offer addOffer(Offer offer);
    Offer expireOffer(long id);
}

package com.mytest.resttest2.service;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.mytest.resttest2.entity.Offer;
import com.mytest.resttest2.repository.OfferRepository;

@RunWith(SpringRunner.class)
public class OfferServiceImplTest {
	
	private static final int ANY_DURATION = 10;
	private static final String ANY_CURRENCY = "GBP";
	private static final BigDecimal ANY_PRICE = BigDecimal.ONE;
	private static final LocalDateTime ANY_TIME = LocalDateTime.now();
	private static final String ANY_DESCRIPTION = "this_description";
	private static final String ANY_EXPIRED_DESCRIPTION = "expired";
	private static final String ANY_NAME = "this_name";

    @TestConfiguration
    static class EmployeeServiceImplTestContextConfiguration {
  
        @Bean
        public OfferService offerService() {
            return new OfferServiceImpl();
        }
    }
    
	@Autowired
	private OfferService offerService;
	
	@MockBean
	private OfferRepository offerRepository;

	private Offer anyOffer;
	private Offer anyExpiredOffer;
	List<Offer> returnedOffers;
	
	@Before
	public void setUp() throws Exception {
		anyOffer = new Offer(ANY_NAME, ANY_DESCRIPTION,
				ANY_TIME, ANY_PRICE, ANY_CURRENCY, ANY_DURATION);
		
		anyExpiredOffer = new Offer(ANY_NAME, ANY_EXPIRED_DESCRIPTION,
				LocalDateTime.now().minusHours(1), ANY_PRICE, ANY_CURRENCY, ANY_DURATION);
		anyExpiredOffer.setEndTime(LocalDateTime.now().minusMinutes(1));
		
		returnedOffers = new ArrayList<>();
		returnedOffers.add(anyOffer);
		returnedOffers.add(anyExpiredOffer);
		
	}

	@After
	public void tearDown() throws Exception {
	}

    @Test
    public void whenAddingValidNew_thenCallsSaveToDB() {
		when(offerRepository.save(any(Offer.class))).thenReturn(anyOffer);
		
		offerService.addOffer(anyOffer);
		
		verify(offerRepository, times(1)).save(anyOffer);
	}

    @Test
    public void whenFetchingAll_thenReturnAllNonExpired() {
    	when(offerRepository.findAll()).thenReturn(returnedOffers);
    	
    	List<Offer> onlyValid = offerService.listOffers();
    	
    	assertTrue(onlyValid.size() == 1);
    	assertTrue(onlyValid.get(0).getDescription().equals(ANY_DESCRIPTION));
 	}
    
    @Test
    public void whenFetchingById_thenReturnSingleOffer() {

    	when(offerRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(anyOffer));
    	
    	Offer offer= offerService.findOffer(1L);
    	
    	assertTrue(offer.equals(anyOffer));
    	verify(offerRepository, times(1)).findById(1L);
 	}    
    
    @Test
    public void whenExpiringOffer_thenFindAndSave() {

    	when(offerRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(anyOffer));
    	when(offerRepository.save(any(Offer.class))).thenReturn(anyOffer);
    	
    	Offer offer= offerService.expireOffer(1L);
    	
    	assertTrue(offer.equals(anyOffer));
    	verify(offerRepository, times(1)).findById(1L);
    	verify(offerRepository, times(1)).save(any(Offer.class));
 	}  
    
    // TODO - I have only tested the happy paths! But you get the idea....

}

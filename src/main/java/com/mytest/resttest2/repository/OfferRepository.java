package com.mytest.resttest2.repository;

import org.springframework.data.repository.CrudRepository;

import com.mytest.resttest2.entity.Offer;

public interface OfferRepository extends CrudRepository<Offer, Long> {
}

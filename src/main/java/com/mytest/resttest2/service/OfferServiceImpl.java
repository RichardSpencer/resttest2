package com.mytest.resttest2.service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mytest.resttest2.entity.Offer;
import com.mytest.resttest2.exception.OfferNotFoundException;
import com.mytest.resttest2.repository.OfferRepository;



@Service
public class OfferServiceImpl implements OfferService {
    private static final String OFFER_NOT_FOUND = "Requested offer not found";
    
	@Autowired
    private OfferRepository offerRepository;

    @Override
    public List<Offer> listOffers() {
    	List<Offer> allOffers = (List<Offer>) offerRepository.findAll();
        return allOffers.stream().filter(x -> x.getEndTime().isAfter(LocalDateTime.now()))
        		.collect(Collectors.toList());
     }

    @Override
    public Offer findOffer(long id) {
        Optional<Offer> optionalOffer = offerRepository.findById(id);

        if(optionalOffer.isPresent())
            return optionalOffer.get();
        else
            throw new OfferNotFoundException(OFFER_NOT_FOUND);
    }

	@Override
	public Offer addOffer(Offer offer) {
		offer.setEndTime(offer.getStartTime().plus(offer.getHours(), ChronoUnit.HOURS));
		offerRepository.save(offer);
		return offer;
	}

	@Override
	public Offer expireOffer(long id) {
        Optional<Offer> optionalOffer = offerRepository.findById(id);

        if(optionalOffer.isPresent()) {
        	Offer offer = optionalOffer.get();
        	offer.setEndTime(LocalDateTime.now());
            offerRepository.save(offer);
            return offer;
        } else {
            throw new OfferNotFoundException(OFFER_NOT_FOUND);
        }
    }	
}



package com.mytest.resttest2.entity;


import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Offer {

	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    @Column(name="offer_id")
	    private Long id;

	    @Column(name = "name", nullable = false)
	    private String name;

	    @Column(length = 2000)
	    private String description;
	    
	    @Column(name="start_time")
	    private LocalDateTime startTime;
	    
	    @Column(name="end_time")
	    private LocalDateTime endTime;
	    
	    @Column(name="offer_price")
	    private BigDecimal offerPrice;
	    
	    @Column(name="currency")
	    private String currency;
	    
	    @Column(name="hours")
	    private Integer hours;
	    
	    

	    public Offer() {
	    }

	    public Offer(String name, String description, LocalDateTime startTime, 
	    		BigDecimal offerPrice, String currency, Integer hours) {
	        this.name = name;
	        this.description = description;
	        this.startTime = startTime;
	        this.endTime = startTime.plus(hours, ChronoUnit.HOURS);
	        this.offerPrice = offerPrice;
	        this.hours = hours;
      		this.currency=currency;
	    }
	    public Long getId() {
	        return id;
	    }

	    public void setId(Long id) {
	        this.id = id;
	    }

	    public String getName() {
	        return name;
	    }

	    public void setName(String name) {
	        this.name = name;
	    }

	    public String getDescription() {
	        return description;
	    }

	    public void setDescription(String description) {

	        this.description = description;
	    }
	    
	    public LocalDateTime getStartTime() {
	        return startTime;
	    }
	    
	    public void setStartTime(LocalDateTime startTime) {
	        this.startTime = startTime;
	    }

	    public LocalDateTime getEndTime() {
	        return endTime;
	    }
	    
	    public void setEndTime(LocalDateTime endTime) {
	        this.endTime = endTime;
	    }

	    public BigDecimal getOfferPrice() {
	        return offerPrice;
	    }
	    
	    public void setOfferPrice(BigDecimal offerPrice) {
	        this.offerPrice = offerPrice;
	    }

	    public String getCurrency() {
	        return currency;
	    }
	    
	    public void setCurrency(String currency) {
	        this.currency = currency;
	    }

	    public Integer getHours() {
	        return hours;
	    }
	    
	    public void setHours(Integer hours) {
	        this.hours = hours;
	    }

	    @Override
	    public String toString() {
	        return "Application{" +
	                "id=" + id +
	                ", name='" + name + '\'' +
	                ", description='" + description + '\'' +
	                ", startTime='" + startTime + '\'' +
	                ", endTime='" + endTime + '\'' +
	                ", offerPrice='" + offerPrice.toString() + '\'' +
	                ", currency='" + currency + '\'' +
	                ", hours='" + hours + '\'' +
	                '}';
	    }
	}
